package kr.ac.mju.mp2019f.hw7;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import java.util.ArrayList;

public class AudioServiceInterface {
    private ServiceConnection serviceConnection;
    private AudioService audioService;

    public AudioServiceInterface(Context context) {
        this.serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                audioService = ((AudioService.AudioServiceBinder) service).getService();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                serviceConnection = null;
                audioService = null;
            }
        };

        // BindService 를 통해서 AudioService 와 직접 연결
        context.bindService(new Intent(context, AudioService.class)
                .setPackage(context.getPackageName()), serviceConnection, Context.BIND_AUTO_CREATE);
    }

    public void play(int position) {
        if (this.audioService != null) {
            this.audioService.play(position);
        }
    }

    public void play() {
        if (this.audioService != null) {
            this.audioService.play();
        }
    }

    public void pause() {
        if (this.audioService != null) {
            this.audioService.pause();
        }
    }

    public void stop() {
        if (this.audioService != null) {
            this.audioService.stop();
        }
    }

    public int togglePlay() {
        int state;

        if (isPlaying()) {
            this.audioService.pause();
            state = 0;
        } else {
            this.audioService.play();
            state = 1;
        }

        return state;
    }

    public boolean isPlaying() {
        if (this.audioService != null) {
            return this.audioService.isPlaying();
        }
        return false;
    }

    public boolean isPause() {
        if (this.audioService != null) {
            return this.audioService.isPause();
        }
        return false;
    }

    public void seekTo(int position) {
        if (this.audioService != null) {
            this.audioService.seekTo(position);
        }
    }

    public int getCurrentPosition() {
        if (this.audioService != null) {
            return this.audioService.getCurrentPosition();
        }
        return 0;
    }

    public int getMusicPosition() {
        return this.audioService.getMusicPosition();
    }
}
