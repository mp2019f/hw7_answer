package kr.ac.mju.mp2019f.hw7.util;

public class Constants {



    /* SharedPreferences 관련 상수 */
    public static class SharedPreferencesName {
        // 1번노래 Play 횟수
        public static final String PLAY_COUNT_1 = "play_count_1";
        // 2번노래 Play 횟수
        public static final String PLAY_COUNT_2 = "play_count_2";
        // 1번노래 좋아요 횟수
        public static final String LIKES_COUNT_1 = "likes_count_1";
        // 2번노래 좋아요 횟수
        public static final String LIKES_COUNT_2 = "likes_count_2";
    }

    /* Broadcast 를 위한 IntentFilter Action */
    public static class IntentFilterAction {
        public static final String PLAY_SEEK_POSITION = "PLAY_SEEK_POSITION";
        public static final String PLAY_COMPLETED = "PLAY_COMPLETED";
    }
}
