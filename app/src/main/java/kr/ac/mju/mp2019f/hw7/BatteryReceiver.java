package kr.ac.mju.mp2019f.hw7;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

public class BatteryReceiver extends BroadcastReceiver {
    private static String TAG = BatteryReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (TextUtils.isEmpty(action)) {
            return;
        }

        Log.d(TAG, "action" + action);

        switch (action) {
            case Intent.ACTION_BATTERY_LOW:
                // 일시중지
                AppClass.getInstance().getServiceInterface().pause();
                break;
            case Intent.ACTION_BATTERY_OKAY:
                // 플레이중이지 않으면 다시재생
                if (!AppClass.getInstance().getServiceInterface().isPlaying()) {
                    AppClass.getInstance().getServiceInterface().play();
                }
                break;
        }
    }
}
