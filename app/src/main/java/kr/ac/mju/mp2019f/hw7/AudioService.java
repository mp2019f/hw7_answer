package kr.ac.mju.mp2019f.hw7;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.util.Timer;
import java.util.TimerTask;

import kr.ac.mju.mp2019f.hw7.util.Constants;

public class AudioService extends Service {
    private final IBinder binder = new AudioServiceBinder();
    private MediaPlayer mediaPlayer;
    private boolean isPrepared;

    private Timer timer;
    private TimerTask timerTask;

    // 노래 위치(1번/2번)
    private int musicPosition;

    // 노래 제목 / 가수 (알림때 사용)
    private String musicTitle;
    private String musicArtist;

    // 서비스에서 제공해주는 public 함수들(아래에서 정의할 함수들)을 사용하기 위한 통신채널
    public class AudioServiceBinder extends Binder {
        AudioService getService() {
            return AudioService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // MediaPlayer 객체 생성
        this.mediaPlayer = new MediaPlayer();

        // 시스템이 휴면모드로 진입하는것을 막기 위함
        this.mediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);

        // 플레이 준비상태
        this.mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                isPrepared = true;

                // Timer 먼저 stop 후 다시 시작
                stopTimerTask();

                // Timer 시작 (1초뒤 시작)
                startTimer();

                mp.start();
            }
        });

        // 재생 완료
        this.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                isPrepared = false;

                stopTimerTask();

                Log.d("AudioService", "onCompletion");

                // 알림 표시
                sendNotification(musicPosition);

                // 완료 Broadcast
                Intent intent = new Intent(Constants.IntentFilterAction.PLAY_COMPLETED);
                intent.putExtra("music_position", musicPosition);
                sendBroadcast(intent);
            }
        });

        // 오류
        this.mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                isPrepared = false;
                return false;
            }
        });

        // Seek 조작 완료
        this.mediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
            @Override
            public void onSeekComplete(MediaPlayer mp) {

            }
        });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        stopTimerTask();

        // MediaPlayer release
        if (this.mediaPlayer != null) {
            this.mediaPlayer.stop();
            this.mediaPlayer.release();
            this.mediaPlayer = null;
        }
    }

    /* play */
    public void play(int position) {
        this.musicPosition = position;

        stop();
        prepare(position);
    }

    /* play */
    public void play() {
        if (this.isPrepared) {
            this.mediaPlayer.start();
        }
    }

    /* 일시정지 */
    public void pause() {
        if (this.isPrepared) {
            this.mediaPlayer.pause();
        }
    }

    /* stop */
    public void stop() {
        this.isPrepared = false;

        stopTimerTask();

        this.mediaPlayer.stop();
        this.mediaPlayer.reset();
    }

    /* play 중인지 체크 */
    public boolean isPlaying() {
        return this.mediaPlayer.isPlaying();
    }

    /* pause 중인지 체크 */
    public boolean isPause() {
        if (isPlaying()) {
            return false;
        } else {
            if (this.isPrepared) {
                // 진행된 상태이면
                if (this.mediaPlayer.getCurrentPosition() > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /* seek 설정 */
    public void seekTo(int position) {
        if (this.isPrepared) {
            // 총재생시간보다 크면
            if (position > this.mediaPlayer.getDuration()) {
                this.mediaPlayer.seekTo(this.mediaPlayer.getDuration());
            } else {
                this.mediaPlayer.seekTo(position);
            }
        }
    }

    /* 현재 노래 진행 위치 */
    public int getCurrentPosition() {
        if (this.isPrepared) {
            return this.mediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    /* 노래 위치(1번/2번) */
    public int getMusicPosition() {
        return this.musicPosition;
    }

    /* 준비상태 */
    private void prepare(int position) {
        try {
            Uri uri = getUri(position);

            // 알림에 사용할 노래 제목 및 가수 설정
            setMusicInfo(uri);

            this.mediaPlayer.setDataSource(getApplicationContext(), uri);
            this.mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            this.mediaPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* 음악파일 url */
    private Uri getUri(int position) {
        String uriPath="android.resource://" + getPackageName() + "/raw/";

        if (position == 0) {
            uriPath += "a";
        } else {
            uriPath += "b";
        }

        return Uri.parse(uriPath);
    }

    /* Timer 시작 */
    private void startTimer() {
        this.timer = new Timer();

        // TimerTask 초기화
        initTimerTask();

        // 1초마다 체크
        this.timer.schedule(timerTask, 1000, 1000);
    }

    /* TimerTask 초기화 */
    private void initTimerTask() {
        this.timerTask = new TimerTask() {
            @Override
            public void run() {
                if (mediaPlayer != null) {
                    if (isPlaying()) {
                        // 현재 진행 위치
                        int currentPosition = mediaPlayer.getCurrentPosition();

                        // 진행 위치를 Broadcast
                        Intent intent = new Intent(Constants.IntentFilterAction.PLAY_SEEK_POSITION);
                        intent.putExtra("music_position", musicPosition);
                        intent.putExtra("play_current_position", currentPosition);
                        sendBroadcast(intent);
                    }
                }
            }
        };
    }

    /* TimerTask stop */
    private void stopTimerTask() {
        if (this.timer != null) {
            this.timer.cancel();
            this.timer = null;
        }
    }

    /* 노래 제목 / 가수 설정 */
    private void setMusicInfo(Uri uri) {
        MediaMetadataRetriever metadataRetriever = new MediaMetadataRetriever();
        metadataRetriever.setDataSource(getApplicationContext(), uri);

        this.musicArtist = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        this.musicTitle = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
    }

    /* 알림 */
    private void sendNotification(int position) {
        // 알림을 선택하면 해당 뮤직화면으로 이동
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("position", position);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = "hw7";
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(this.musicTitle)
                .setContentText(this.musicArtist + " - " + this.musicTitle)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Oreo(26) 버전 이후 버전부터는 channel 이 필요함
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = notificationManager.getNotificationChannel(channelId);
            if (channel == null) {
                channel = new NotificationChannel(channelId, "HW7 Music", NotificationManager.IMPORTANCE_DEFAULT);
                notificationManager.createNotificationChannel(channel);
            }
        }

        // 알림 표시
        notificationManager.notify(0, notificationBuilder.build());
    }
}
