package kr.ac.mju.mp2019f.hw7;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import kr.ac.mju.mp2019f.hw7.util.Constants;
import kr.ac.mju.mp2019f.hw7.util.SharedPreferencesUtils;

public class MainActivity extends AppCompatActivity {
    private static String TAG = MainActivity.class.getSimpleName();

    private ImageView imgPhoto1, imgPhoto2;
    private TextView txtArtist1, txtArtist2, txtTitle1, txtTitle2;
    private TextView txtPlayCount1, txtPlayCount2, txtLikesCount1, txtLikesCount2;

    private static final int MUSIC_REQUEST_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int position;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            position = bundle.getInt("position", -1);
            Log.d(TAG, "position: " + position);
        } else {
            position = -1;
        }

        // 제목 표시
        setTitle(getString(R.string.activity_title_main));

        this.txtArtist1 = findViewById(R.id.txtArtist1);
        this.txtArtist2 = findViewById(R.id.txtArtist2);
        this.txtTitle1 = findViewById(R.id.txtTitle1);
        this.txtTitle2 = findViewById(R.id.txtTitle2);

        this.txtPlayCount1 = findViewById(R.id.txtPlayCount1);
        this.txtPlayCount2 = findViewById(R.id.txtPlayCount2);
        this.txtLikesCount1 = findViewById(R.id.txtLikesCount1);
        this.txtLikesCount2 = findViewById(R.id.txtLikesCount2);

        this.imgPhoto1 = findViewById(R.id.imgPhoto1);
        this.imgPhoto2 = findViewById(R.id.imgPhoto2);

        this.imgPhoto1.setOnClickListener(mClickListener);
        this.imgPhoto2.setOnClickListener(mClickListener);

        // 초기화
        init();

        if (position != -1) {
            // 음악 듣기 화면으로 이동
            goMusic(position);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case MUSIC_REQUEST_CODE:
                    // play 횟수 및 좋아요 횟수 다시 표시
                    int position = data.getIntExtra("position", 0);
                    if (position == 0) {
                        displayScreen1();
                    } else {
                        displayScreen2();
                    }
                    break;
            }
        }
    }

    /* 초기화 */
    private void init() {
        // 1번노래 정보
        infoMusic(0);

        // 2번노래 정보
        infoMusic(1);

        // 1번노래 Play 횟수 및 좋아요 횟수 표시
        displayScreen1();

        // 2번노래 Play 횟수 및 좋아요 횟수 표시
        displayScreen2();
    }

    /* 음악파일 정보 */
    private void infoMusic(int position) {
        MediaMetadataRetriever metadataRetriever = new MediaMetadataRetriever();

        // 리소스 파일에서 부터 정보 얻기 위함
        String uriPath="android.resource://" + getPackageName() + "/raw/";
        if (position == 0) {
            uriPath += "a";
        } else {
            uriPath += "b";
        }

        Uri uri = Uri.parse(uriPath);
        metadataRetriever.setDataSource(getApplicationContext(), uri);

        // 가수 와 제목 얻기
        String artist =  metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        String title = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);

        if (position == 0) {
            this.txtArtist1.setText(artist);
            this.txtTitle1.setText(title);
        } else {
            this.txtArtist2.setText(artist);
            this.txtTitle2.setText(title);
        }

        // 이미지 가져오기
        byte[] bytes = metadataRetriever.getEmbeddedPicture();
        if (position == 0) {
            if (bytes != null) {
                imgPhoto1.setImageBitmap(BitmapFactory.decodeByteArray(bytes, 0, bytes.length));
            } else {
                imgPhoto1.setImageResource(R.drawable.a);
            }
        } else {
            if (bytes != null) {
                imgPhoto2.setImageBitmap(BitmapFactory.decodeByteArray(bytes, 0, bytes.length));
            } else {
                imgPhoto2.setImageResource(R.drawable.b);
            }
        }
    }

    /* 1번노래 Play 횟수 및 좋아요 횟수 표시 */
    private void displayScreen1() {
        SharedPreferencesUtils preferencesUtils = SharedPreferencesUtils.getInstance(getApplicationContext());

        // 1번노래 Play 횟수
        int playCount1 = preferencesUtils.get(Constants.SharedPreferencesName.PLAY_COUNT_1, 0);
        // 1번노래 좋아요 횟수
        int likesCount1 = preferencesUtils.get(Constants.SharedPreferencesName.LIKES_COUNT_1, 0);

        this.txtPlayCount1.setText(String.valueOf(playCount1));
        this.txtLikesCount1.setText(String.valueOf(likesCount1));
    }

    /* 2번노래 Play 횟수 및 좋아요 횟수 표시 */
    private void displayScreen2() {
        SharedPreferencesUtils preferencesUtils = SharedPreferencesUtils.getInstance(getApplicationContext());

        // 2번노래 Play 횟수
        int playCount2 = preferencesUtils.get(Constants.SharedPreferencesName.PLAY_COUNT_2, 0);
        // 2번노래 좋아요 횟수
        int likesCount2 = preferencesUtils.get(Constants.SharedPreferencesName.LIKES_COUNT_2, 0);

        this.txtPlayCount2.setText(String.valueOf(playCount2));
        this.txtLikesCount2.setText(String.valueOf(likesCount2));
    }

    /* 음악 듣기 화면으로 이동 */
    private void goMusic(int position) {
        String artist = "";
        String title = "";

        if (position == 0) {
            artist = txtArtist1.getText().toString();
            title = txtTitle1.getText().toString();
        } else {
            artist = txtArtist2.getText().toString();
            title = txtTitle2.getText().toString();
        }

        Log.d(TAG, "title: " + title);

        // 음악 듣기 화면으로 이동
        Intent intent = new Intent(MainActivity.this, MusicActivity.class);
        intent.putExtra("position", position);
        intent.putExtra("music_artist", artist);
        intent.putExtra("music_title", title);

        startActivityForResult(intent, MUSIC_REQUEST_CODE);
    }

    /* 클릭 리스너 */
    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.imgPhoto1:
                    // 첫번째 노래
                    goMusic(0);
                    break;
                case R.id.imgPhoto2:
                    // 두번째 노래
                    goMusic(1);
                    break;
            }


        }
    };
}
