package kr.ac.mju.mp2019f.hw7;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import kr.ac.mju.mp2019f.hw7.util.Constants;
import kr.ac.mju.mp2019f.hw7.util.SharedPreferencesUtils;
import kr.ac.mju.mp2019f.hw7.util.Utils;

public class MusicActivity extends AppCompatActivity {
    private static String TAG = MusicActivity.class.getSimpleName();

    private ImageView imgTogglePlay;
    private TextView txtTime, txtTotalTime, txtLikesCount;

    private SeekBar seekBar;

    // 총재생시간 (ms)
    private int totalTime;

    // 노래 위치(1번/2번)
    private int musicPosition;

    // 노래가 stop 상태인지
    private boolean isStop = false;

    // 최초 play 인지
    private boolean isFirst = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        // 노래 정보
        Intent intent = getIntent();
        // 노래 위치 (1번 / 2번)
        this.musicPosition = intent.getIntExtra("position", 0);
        String musicArtist = intent.getStringExtra("music_artist");
        String musicTitle = intent.getStringExtra("music_title");

        // 제목 표시
        setTitle(getString(R.string.activity_title_music));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.seekBar = findViewById(R.id.seekBar);
        this.seekBar.setOnSeekBarChangeListener(mSeekBarChangeListener);

        this.txtTime = findViewById(R.id.txtTime);
        this.txtTotalTime = findViewById(R.id.txtTotalTime);
        this.txtLikesCount = findViewById(R.id.txtLikesCount);
        this.imgTogglePlay = findViewById(R.id.imgTogglePlay);

        this.imgTogglePlay.setOnClickListener(mClickListener);
        findViewById(R.id.imgStop).setOnClickListener(mClickListener);
        findViewById(R.id.imgLikes).setOnClickListener(mClickListener);

        // 가수
        ((TextView) findViewById(R.id.txtArtist)).setText(musicArtist);
        // 제목
        ((TextView) findViewById(R.id.txtTitle)).setText(musicTitle);

        // 초기화
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // BroadcastReceiver 등록
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.IntentFilterAction.PLAY_SEEK_POSITION);
        filter.addAction(Constants.IntentFilterAction.PLAY_COMPLETED);
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // BroadcastReceiver 해제
        unregisterReceiver(mReceiver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /* 초기화 */
    private void init() {
        // 음악 앨범 정보
        displayInfo(this.musicPosition);

        // 좋아요 표시
        displayLikes(this.musicPosition);

        // 현재 서비스 노래 위치 얻기
        int position = AppClass.getInstance().getServiceInterface().getMusicPosition();
        if (position == this.musicPosition) {
            // 현재 노래 진행상태 표시
            if (AppClass.getInstance().getServiceInterface().isPlaying()) {
                // 노래중...
                displayState(1);

                // SeekBar Lock false
                this.seekBar.setEnabled(true);

                this.isFirst = false;
                this.isStop = false;
            } else {
                // 일시정지 상태인지 체크
                if (AppClass.getInstance().getServiceInterface().isPause()) {
                    displayState(0);

                    // SeekBar Lock false
                    this.seekBar.setEnabled(true);

                    // 노래 SeekBar 표시 및 진행시간 표시
                    displaySeekBar(AppClass.getInstance().getServiceInterface().getCurrentPosition());

                    this.isFirst = false;
                    this.isStop = false;
                }
            }
        }
    }

    /* 음악 앨범 정보 */
    private void displayInfo(int position) {
        MediaMetadataRetriever metadataRetriever = new MediaMetadataRetriever();

        // 리소스 파일에서 부터 정보 얻기 위함
        String uriPath="android.resource://" + getPackageName() + "/raw/";
        if (position == 0) {
            uriPath += "a";
        } else {
            uriPath += "b";
        }

        Uri uri = Uri.parse(uriPath);
        metadataRetriever.setDataSource(getApplicationContext(), uri);

        // 진행시간 default
        this.txtTime.setText("0:00");

        // 노래 총시간
        String time = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        if (!TextUtils.isEmpty(time)) {
            // 총시간
            this.totalTime = Integer.parseInt(time);
            this.txtTotalTime.setText(Utils.getDisplayTime(this.totalTime));

            // SeekBar Max 설정
            this.seekBar.setMax(this.totalTime / 1000);
        }

        // SeekBar Lock
        this.seekBar.setEnabled(false);

        // 이미지 가져오기
        byte[] bytes = metadataRetriever.getEmbeddedPicture();
        if (bytes != null) {
            ((ImageView) findViewById(R.id.imgPhoto)).setImageBitmap(BitmapFactory.decodeByteArray(bytes, 0, bytes.length));
        } else {
            if (position == 0) {
                ((ImageView) findViewById(R.id.imgPhoto)).setImageResource(R.drawable.a);
            } else {
                ((ImageView) findViewById(R.id.imgPhoto)).setImageResource(R.drawable.b);
            }
        }
    }

    /* SeekBar 표시 및 진행시간 표시 */
    private void displaySeekBar(int currentPosition) {
        int progress = currentPosition / 1000;
        if (progress <= this.seekBar.getMax()) {
            this.seekBar.setProgress(currentPosition / 1000);
            this.txtTime.setText(Utils.getDisplayTime(currentPosition));

            if (currentPosition < this.totalTime) {
                this.txtTotalTime.setText(Utils.getDisplayTime(this.totalTime - currentPosition));
            } else {
                this.txtTotalTime.setText("0:00");
            }
        }
    }

    /* 좋아요 횟수 표시 */
    private void displayLikes(int position) {
        SharedPreferencesUtils preferencesUtils = SharedPreferencesUtils.getInstance(getApplicationContext());

        // 좋아요 횟수
        int likesCount;
        if (position == 0) {
            likesCount = preferencesUtils.get(Constants.SharedPreferencesName.LIKES_COUNT_1, 0);
        } else {
            likesCount = preferencesUtils.get(Constants.SharedPreferencesName.LIKES_COUNT_2, 0);
        }

        this.txtLikesCount.setText(String.valueOf(likesCount));
    }

    /* 재생 및 일시정지 */
    private void togglePlay() {
        if (this.isFirst) {
            // 최초 Play
            SharedPreferencesUtils preferencesUtils = SharedPreferencesUtils.getInstance(getApplicationContext());

            // 재생 횟수 증가
            int playCount;
            if (this.musicPosition == 0) {
                playCount = preferencesUtils.get(Constants.SharedPreferencesName.PLAY_COUNT_1, 0);
                preferencesUtils.put(Constants.SharedPreferencesName.PLAY_COUNT_1, ++playCount);
            } else {
                playCount = preferencesUtils.get(Constants.SharedPreferencesName.PLAY_COUNT_2, 0);
                preferencesUtils.put(Constants.SharedPreferencesName.PLAY_COUNT_2, ++playCount);
            }

            // 재생횟수 update 를 위해 메인에 전달
            sendResult();

            // SeekBar unLock
            this.seekBar.setEnabled(true);

            // 노래 재생
            AppClass.getInstance().getServiceInterface().play(this.musicPosition);
            displayState(1);

            this.isFirst = false;
            this.isStop = false;
        } else {
            if (this.isStop) {
                // stop 상태이면 play

                // SeekBar unLock
                this.seekBar.setEnabled(true);

                // 노래 재생
                AppClass.getInstance().getServiceInterface().play(this.musicPosition);
                displayState(1);
                this.isStop = false;
            } else {
                displayState(AppClass.getInstance().getServiceInterface().togglePlay());
            }
        }
    }

    /* stop */
    private void stop() {
        AppClass.getInstance().getServiceInterface().stop();
        displayState(0);
        // 노래 SeekBar 표시 및 진행시간 표시 초기화
        displaySeekBar(0);

        // SeekBar Lock
        this.seekBar.setEnabled(false);

        this.isStop = true;
    }

    /* play 상태 (아이콘 play / pause(stop)) 표시 */
    private void displayState(int state) {
        if (state == 0) {
            // pause (stop) 상태
            this.imgTogglePlay.setImageResource(R.drawable.ic_play_circle_48_blue);
        } else {
            // play 상태
            this.imgTogglePlay.setImageResource(R.drawable.ic_pause_circle_48_blue);
        }
    }

    /* 좋아요 추가 */
    private void addLikesCount() {
        SharedPreferencesUtils preferencesUtils = SharedPreferencesUtils.getInstance(getApplicationContext());

        int likesCount = Integer.parseInt(this.txtLikesCount.getText().toString());
        if (this.musicPosition == 0) {
            preferencesUtils.put(Constants.SharedPreferencesName.LIKES_COUNT_1, ++likesCount);
        } else {
            preferencesUtils.put(Constants.SharedPreferencesName.LIKES_COUNT_2, ++likesCount);
        }

        this.txtLikesCount.setText(String.valueOf(likesCount));

        // 메인에 전달
        sendResult();
    }

    /* 메인에 전달 */
    private void sendResult() {
        Intent intent = new Intent();
        intent.putExtra("position", this.musicPosition);
        setResult(Activity.RESULT_OK, intent);
    }

    SeekBar.OnSeekBarChangeListener mSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        // 조작전 상탱 (0:pause, 1:play)
        int state = 0;

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            // seekBar 진행바가 max 값이면 Lock
            if (seekBar.getMax() == progress) {
                if (fromUser) {
                    // 조작으로 인해 발생함
                } else {
                    seekBar.setEnabled(false);
                }
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            // SeekBar 조작 시작
            if (AppClass.getInstance().getServiceInterface().isPlaying()) {
                state = 1;

                // 조작전에 일시정지 시킴
                AppClass.getInstance().getServiceInterface().pause();
            } else {
                state = 0;
            }
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            // SeekBar 조작 끝
            AppClass.getInstance().getServiceInterface().seekTo(seekBar.getProgress() * 1000);
            // play 중이였으면
            if (state == 1) {
                AppClass.getInstance().getServiceInterface().play();
            }
        }
    };

    View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.imgTogglePlay:
                    // 재생 일시정지
                    togglePlay();
                    break;
                case R.id.imgStop:
                    // 정지
                    stop();
                    break;
                case R.id.imgLikes:
                    // 좋아요
                    addLikesCount();
                    break;
            }
        }
    };

    BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch (action) {
                case Constants.IntentFilterAction.PLAY_SEEK_POSITION:
                    // 진행바
                    int position1 = intent.getIntExtra("music_position", -1);
                    int currentPosition = intent.getIntExtra("play_current_position", 0);

                    Log.d(TAG, "position:" + position1);
                    Log.d(TAG, "currentPosition:" + currentPosition);

                    // 해당 노래이면
                    if (position1 == musicPosition) {
                        // 노래 SeekBar 표시 및 진행시간 표시
                        displaySeekBar(currentPosition);
                    }
                    break;
                case Constants.IntentFilterAction.PLAY_COMPLETED:
                    // 재생완료
                    int position2 = intent.getIntExtra("music_position", -1);

                    // 해당 노래이면
                    if (position2 == musicPosition) {
                        // 조작에 의한 재생 완료이면
                        if (seekBar.isEnabled()) {
                            seekBar.setEnabled(false);

                            // 재생시간을 총시간으로 표시
                            txtTime.setText(Utils.getDisplayTime(totalTime));
                            // 남은시간은 0
                            txtTotalTime.setText("0:00");
                        }
                    }
                    break;
            }
        }
    };
}
