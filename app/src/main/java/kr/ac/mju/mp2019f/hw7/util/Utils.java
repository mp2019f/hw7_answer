package kr.ac.mju.mp2019f.hw7.util;

import java.util.Locale;

public class Utils {

    /* 시간 표현 값 얻기 */
    public static String getDisplayTime(long millis) {
        int h = (int) (millis / 3600000);
        int m = (int) (millis / 60000) % 60000;
        // 초는 반올림
        int s = Math.round(((float) millis % 60000) / 1000);

        String str;
        if (h == 0) {
            str = String.format(Locale.getDefault(), "%01d:%02d", m, s);
        } else {
            str = String.format(Locale.getDefault(), "%01d:%01d:%02d", h,  m, s);
        }

        return str;
    }
}
