package kr.ac.mju.mp2019f.hw7;

import android.app.Application;

public class AppClass extends Application {
    private static AppClass _instance;
    private AudioServiceInterface _interface;

    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;

        // AudioServiceInterface 클래스를 한번만 생성하기 위함
        _interface = new AudioServiceInterface(getApplicationContext());
    }

    public static AppClass getInstance() {
        return _instance;
    }

    public AudioServiceInterface getServiceInterface() {
        return _interface;
    }
}
